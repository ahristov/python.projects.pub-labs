import string

s = 'The quick brown fox jumped over the lazy dog.'

print s

print s.capitalize()
print string.capitalize(s)
print string.capwords(s)

leet = string.maketrans("bro", "123")
print s.translate(leet)

values = { 'var' : 'foo' }

t = string.Template("""

Variable            : $var
Escape              : $$
Variable in text    : ${var}siable

""")

print 'TEMPLATE:', t.substitute(values)

print string.Template('$who likes $what').safe_substitute({'who' : 'tim'})

print string.Template("""

    SELECT  *
    FROM    my_customers c
    WHERE   c.id = $id
            OR c.name = '$name'

""").safe_substitute({
    'id'     : 100,
    'name'     : 'Atanas'
})


t = string.Template("$notdefined")

try:
    print 'substitute():', t.substitute(values)
except KeyError, err:
    print 'KEYERROR:', str(err)
except Error, err:
    print 'ERRROR:', str(err)

print 'safe_substitute():', t.safe_substitute(values)


